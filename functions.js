
//DOM Elements
const emojiAnimalElement = document.getElementById("input-emoji-animal");
const animalNameElement = document.getElementById("input-animal-name");
const buttonCreateAnimalElement = document.getElementById("create-animal");
const listEmojiAnimalElement = document.getElementById("li-emoji-animal");


//Function constructor for animal

function Animal(id, emoji, name){

  this.id = id;
  this.emoji = emoji;
  this.name = name;


}

// Click button to add emoji animal
function onButtonClick(){

createEmojiAnimal();

}

//EventListener
buttonCreateAnimalElement.addEventListener("click", onButtonClick);

// Create Id of the emoji animal
function createId(numberOfAnimal){

  return Math.random().toString(16).slice(2);
  
  }
  

// Create emoji of the animal

function createEmojiAnimal() {

  // Id for the animal
  const newId = createId();

  // Create object of the emoji animal
  const animal = new Animal(newId, emojiAnimalElement.value.trim(), animalNameElement.value.trim());
 
  // const listOfAnimal = [animal];
  const listOfAnimal = [animal];

  for (var i = 0; i < listOfAnimal.length; i++) {
    // Create DOM element
    const li = document.createElement('li');
    // Set text of element
    li.textContent = listOfAnimal[i].name;

    // Append this element to its parent
    listEmojiAnimalElement.appendChild(li);
  }
}
